package com.example.chatarina.todolist;

/**
 * Created by Chatarina on 2/26/17.
 */

import android.content.Context;
import android.content.Intent;
import android.support.test.InstrumentationRegistry;
import android.support.test.annotation.UiThreadTest;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.example.chatarina.todolist.viewmodels.EditTaskViewModel;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;


@RunWith(AndroidJUnit4.class)
public class EditViewTests {
    @Rule
    public ActivityTestRule<EditTaskActivity> mActivityRule = new ActivityTestRule<>(EditTaskActivity.class, true , false);

    @Rule
    public ActivityTestRule<MainActivity> mMainActivityRule = new ActivityTestRule<>(MainActivity.class);

    @Before
    public void setup() {

    }
    @Test
    public void Test_TitleDisplayedCorrectly_ManualIntent(){
        Intent intent = new Intent();
        intent.putExtra("START_REASON", Constants.ADD_TASK_REQUEST_CODE);

        mActivityRule.launchActivity(intent);

        onView(withId(R.id.title_text)).check(matches(withText("Add Task")));
    }

    @Test
    public void Test_TitleDisplayedCorrectly(){
        mMainActivityRule.getActivity().showAddView();

        onView(withId(R.id.title_text)).check(matches(withText("Add Task")));
    }

    @Test
    public void Test_TitleDisplayeCorrectly_Editing(){
        mMainActivityRule.getActivity().showEditView();

        onView(withId(R.id.title_text)).check(matches(withText("Edit Task")));
    }
}
