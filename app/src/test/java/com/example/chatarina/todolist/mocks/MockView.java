package com.example.chatarina.todolist.mocks;

/**
 * Created by Chatarina on 2/20/17.
 */

import com.example.chatarina.todolist.interfaces.IView;
import com.example.chatarina.todolist.models.Task;

public class MockView implements IView {

    public boolean displayTaskWasCalled;
    public boolean displayedNextTask;
    public Task displayedTask;

    @Override
    public void displayTask(Task task) {
        displayTaskWasCalled = true;
        displayedTask = task;
    }

    @Override
    public void displayNextTask(Task task) {
        displayedNextTask = true;
    }

    @Override
    public void showAddView(){

    }

    @Override
    public void showEditView(){

    }
    @Override
    public void handleMove(int num, int num2){

    }
    @Override
    public void handleDelete(int num){

    }
    @Override
    public void showToast(String text, int num){

    }
    @Override
    public void handleAdd(int num){

    }
}
