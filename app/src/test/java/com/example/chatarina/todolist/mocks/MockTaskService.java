package com.example.chatarina.todolist.mocks;

import java.util.List;


import com.example.chatarina.todolist.interfaces.ITaskService;
import com.example.chatarina.todolist.models.Task;


public class MockTaskService implements ITaskService {

    public List<Task> tasks;

    public MockTaskService(List<Task> tasks){
        this.tasks = tasks;
    }

    @Override
    public List<Task> getTasks() {
        return tasks;
    }
}