package com.example.chatarina.todolist.mocks;

/**
 * Created by Chatarina on 2/20/17.
 */


import com.example.chatarina.todolist.interfaces.IPresenter;
import com.example.chatarina.todolist.models.Task;


public class MockPresenter implements IPresenter {
    @Override
    public void moveToPrevTask() {

    }

    @Override
    public void moveToNextTask() {

    }

    @Override
    public Task getCurrentTask() {
        return null;
    }

    @Override
    public void markCurrentTaskImportant() {

    }

    @Override
    public Task getNextTask() {
        return null;
    }

    @Override
    public void showAddOrEditView(Task task){

    }
}

