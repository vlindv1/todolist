package com.example.chatarina.todolist;

/**
 * Created by Chatarina on 2/20/17.
 */
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import com.example.chatarina.todolist.models.Task;
import com.example.chatarina.todolist.services.TaskService;
import com.example.chatarina.todolist.interfaces.IView;
import com.example.chatarina.todolist.presenters.Presenter;
import com.example.chatarina.todolist.interfaces.IPresenter;
import com.example.chatarina.todolist.models.TaskModel;
import com.example.chatarina.todolist.interfaces.ITaskService;
import com.example.chatarina.todolist.mocks.MockPresenter;
import com.example.chatarina.todolist.mocks.MockTaskService;
import com.example.chatarina.todolist.mocks.MockView;


public class PresenterTest {
    IView view;

    ITaskService TaskService;
    @Test
    public void Test_MarkingTaskImportant() {
        List<Task> tasks = makeTestTasks();

        TaskService = new MockTaskService(tasks);
        view = new MockView();

        // create presenter
        IPresenter presenter = new Presenter(view, new TaskModel(TaskService));

        // now move to the previous task
        presenter.markCurrentTaskImportant();

        // assert that it is the expected task
        Assert.assertTrue(presenter.getCurrentTask().getIsImportant());
    }

    @Test
    public void Test_DisplayMessageWhenNoTasks() {

        List<Task> tasks = new ArrayList<>();

        TaskService = new MockTaskService(tasks);
        view = new MockView();

        IPresenter presenter = new Presenter(view, new TaskModel(TaskService));

        presenter.moveToNextTask();

        Task currentTask = presenter.getCurrentTask();

        Assert.assertEquals(null, currentTask);
    }

    @Test
    public void Test_ShowsNextTask() {

        List<Task> tasks = makeTestTasks();

        // setup model
        TaskService = new MockTaskService(tasks);
        view = new MockView();

        // create presenter
        IPresenter presenter = new Presenter(view, new TaskModel(TaskService));

        // get first task
        Task task = presenter.getNextTask();

        view.displayNextTask(task);
        MockView mockView = (MockView)view;
        Assert.assertEquals(true, mockView.displayedNextTask);
    }
    @Test
    public void Test_GetCurrentTask() {
        List <Task> tasks = makeTestTasks();

        TaskService = new MockTaskService(tasks);
        view = new MockView();

        IPresenter presenter = new Presenter(view, new TaskModel(TaskService));

        Task task = presenter.getCurrentTask();

        view.displayTask(task);
        MockView mockView = (MockView)view;
    }


    private List<Task> makeTestTasks() {
        List<Task> tasks = new ArrayList<>();
        Task task = new Task();
        task.setTitle("Test");
        tasks.add(task);
        task = new Task();
        task.setTitle("Test2");
        tasks.add(task);
        return tasks;
    }

}
