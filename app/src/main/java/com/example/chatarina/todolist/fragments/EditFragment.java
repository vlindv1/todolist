package com.example.chatarina.todolist.fragments;

/**
 * Created by Chatarina on 3/13/17.
 */

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.chatarina.todolist.models.Task;
//import com.example.chatarina.todolist.EditTaskActivity;
import com.example.chatarina.todolist.R;
import com.example.chatarina.todolist.interfaces.IEditPresenter;
import com.example.chatarina.todolist.interfaces.IEditView;
import com.example.chatarina.todolist.interfaces.IView;
import com.example.chatarina.todolist.presenters.EditTaskPresenter;
import com.example.chatarina.todolist.viewmodels.EditTaskViewModel;

/**
 * A simple {@link Fragment} subclass.
 */
public class EditFragment extends Fragment implements IEditView, View.OnClickListener {

    //presenter
    IEditPresenter presenter;

    //widgets
    TextView titleTv;
    TextView viewModelTv;
    EditText titleEt;
    Button imptBox;
    EditText startDate;
    EditText dueDate;
    EditText descriptionEt;
    Button saveBtn;

    public EditFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_edit, container, false);

        titleTv = (TextView)view.findViewById(R.id.title_text);
        titleEt = (EditText)view.findViewById(R.id.titleEditText);
        descriptionEt = (EditText)view.findViewById(R.id.descriptionEditText);
        imptBox = (Button)view.findViewById(R.id.imptCheckBox);
        startDate = (EditText)view.findViewById(R.id.startViewEdit);
        dueDate = (EditText)view.findViewById(R.id.dueViewEdit);
        //viewModelTv = (TextView)view.findViewById(R.id.debug_model);
        saveBtn = (Button)view.findViewById(R.id.save_button);

        titleEt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2){

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2){

            }
            @Override
            public void afterTextChanged(Editable editable){
                presenter.updateTaskTitle(editable.toString());
            }
        });
        descriptionEt.addTextChangedListener(new TextWatcher(){
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2){

            }
            @Override
            public void onTextChanged(CharSequence charSequence, int i , int i1, int i2){

            }
            @Override
            public void afterTextChanged(Editable editable){
                presenter.updateTaskDescription(editable.toString());
            }
        });
        startDate.addTextChangedListener(new TextWatcher(){
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2){

            }
            @Override
            public void onTextChanged(CharSequence charSequence, int i , int i1, int i2){

            }
            @Override
            public void afterTextChanged(Editable editable){
                presenter.updateStartDate(editable.toString());
            }
        });
        dueDate.addTextChangedListener(new TextWatcher(){
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2){

            }
            @Override
            public void onTextChanged(CharSequence charSequence, int i , int i1, int i2){

            }
            @Override
            public void afterTextChanged(Editable editable){
                presenter.updateDueDate(editable.toString());
            }
        });
        saveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Task newTask = new Task();
                newTask.setTitle(titleEt.getText().toString());
                newTask.setDescription(descriptionEt.getText().toString());
                newTask.setStartDate(startDate.getText().toString());
                newTask.setDueDate(dueDate.getText().toString());

                presenter.setModel(newTask);

                presenter.save();
                clear();
            }
        });
        return view;
    }
    //not used
    public static Intent newIntent(Context context){
        Intent intent = new Intent (context, EditFragment.class);
        return intent;
    }

    @Override
    public void displayTitle(int title){
        titleTv.setText(title);
    }

    @Override
    public void updateViewWithViewModel(EditTaskViewModel vm){
        //viewModelTv.setText("Title: " + vm.title + "\nDescription: " + vm.description + "\nStart Date: " + vm.startDate + "\nDue Date: " + vm.dueDate);
    }
    @Override
    public void returnResult(EditTaskViewModel viewModel){
        Intent intent = viewModel.makeIntent();
        ((Activity)getContext()).setResult(Activity.RESULT_OK, intent);
        ((Activity)getContext()).finish();
        clear();
    }

    @Override
    public void displayInvalid(EditTaskViewModel viewModel){
        viewModelTv.setText("View model is not valid");
    }

    @Override
    public void setInitialFields(EditTaskViewModel viewModel){
        titleEt.setText(viewModel.title);
        descriptionEt.setText(viewModel.description);
        startDate.setText(viewModel.startDate);
        dueDate.setText(viewModel.dueDate);
    }

    @Override
    public void sendTaskBack(Intent intent){
        ((Activity)getContext()).setResult(Activity.RESULT_OK, intent);
        ((Activity)getContext()).finish();
    }

    @Override
    public void onClick(View view){
        switch(view.getId()){
            case R.id.save_button:
                presenter.validateModel();
                //Log.d(TAG, titleEt.getText().toString() + " " + descriptionEt.getText().toString() + " " + startDate.getText().toString() + " " + dueDate.getText().toString());
                presenter.saveTask(titleEt.getText().toString(), descriptionEt.getText().toString(), false, false, startDate.getText().toString(), dueDate.getText().toString());
                break;
        }
    }

    public void clear(){
        titleEt.setText("");
        descriptionEt.setText("");
        startDate.setText("");
        dueDate.setText("");
    }
    public void setPresenter(IEditPresenter presenter) {
        this.presenter = presenter;
    }
}
