package com.example.chatarina.todolist.fragments;

/**
 * Created by Chatarina on 3/13/17.
 */

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.chatarina.todolist.Constants;
import com.example.chatarina.todolist.EditTaskActivity;
import com.example.chatarina.todolist.models.Task;
import com.example.chatarina.todolist.presenters.DemoAdapter;

import com.example.chatarina.todolist.R;
import com.example.chatarina.todolist.interfaces.IPresenter;
import com.example.chatarina.todolist.interfaces.IView;

import java.io.InputStream;
import java.util.List;
/**
 * A simple {@link Fragment} subclass.
 */
public class MainFragment extends Fragment implements IView, View.OnClickListener {


    private IPresenter presenter;
    Button prevBtn;
    Button nextBtn;
    Button addBtn;
    Button editBtn;
    TextView TaskTitleTv;
    TextView TaskDescriptionTv;
    TextView startTv;
    TextView dueTv;
    TextView nextTaskTv;
    ImageView taskImg;
    CheckBox importantChkBx;
    LinearLayout taskLayout;

    RecyclerView recyclerView;
    Toast toast;

    DemoAdapter adapter;
    public static final int REQUEST = 1;

    public MainFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_main, container, false);

        TaskTitleTv = (TextView)view.findViewById(R.id.taskTitle);
        TaskDescriptionTv = (TextView)view.findViewById(R.id.taskDescription);
        startTv = (TextView)view.findViewById(R.id.startView);
        dueTv = (TextView) view.findViewById(R.id.dueView);
        importantChkBx = (CheckBox) view.findViewById(R.id.imptCheckBox);
        prevBtn = (Button) view.findViewById(R.id.prevBtn);
        nextBtn = (Button) view.findViewById(R.id.nextBtn);
        addBtn = (Button) view.findViewById(R.id.addBtn);
        editBtn = (Button) view.findViewById(R.id.editBtn);
        taskLayout = (LinearLayout)view.findViewById(R.id.task_linear_layout);
        nextTaskTv = (TextView)view.findViewById(R.id.nextTaskTv);
        taskImg = (ImageView)view.findViewById(R.id.taskImg);
        prevBtn.setOnClickListener(this);
        nextBtn.setOnClickListener(this);
        addBtn.setOnClickListener(this);
        editBtn.setOnClickListener(this);
        taskLayout.setOnClickListener(this);

        recyclerView = (RecyclerView)view.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        if(presenter != null)
            presenter.getCurrentTask();
        return view;
    }

    @Override
    public void onClick(View view) {
        switch(view.getId()) {
            case R.id.task_linear_layout:
                presenter.markCurrentTaskImportant();
                break;
            case R.id.addBtn:
                presenter.showAddOrEditView(null);
                break;
//            case R.id.editBtn:
//                presenter.showAddOrEditView(presenter.getCurrentTask());
        }
    }

    public void setPresenter(IPresenter presenter) {
        this.presenter = presenter;

        adapter = new DemoAdapter(presenter);
        adapter.notifyDataSetChanged();
        recyclerView.setAdapter(adapter);
        presenter.getCurrentTask();
    }

    @Override

    public void handleEdit(){
        adapter.notifyDataSetChanged();
        recyclerView.scrollToPosition(0);
    }

    @Override
    public void showEditView(){
        Intent intent = new Intent(getContext(), EditTaskActivity.class);
        intent.putExtra("TITLE", presenter.getCurrentTask().getTitle());
        intent.putExtra("DESCRIPTION", presenter.getCurrentTask().getDescription());
        intent.putExtra("ISCOMPLETE", presenter.getCurrentTask().getIsComplete());
        intent.putExtra("ISIMPORTANT", presenter.getCurrentTask().getIsImportant());
        intent.putExtra("STARTDATE", presenter.getCurrentTask().getStartDate());
        intent.putExtra("DUEDATE", presenter.getCurrentTask().getDueDate());
        intent.putExtra("START_REASON", Constants.EDIT_TASK_REQUEST_CODE);
        ((Activity)getContext()).startActivityForResult(intent, Constants.EDIT_TASK_REQUEST_CODE);
    }

    @Override
    public void showAddView(){
        Intent intent = new Intent(getContext(), EditTaskActivity.class);
        intent.putExtra("START_REASON", Constants.ADD_TASK_REQUEST_CODE);

        ((Activity)getContext()).startActivityForResult(intent, Constants.ADD_TASK_REQUEST_CODE);
    }

    @Override
    public void handleDelete(int position) {
        adapter.notifyItemRemoved(position);
        recyclerView.scrollToPosition(0);
    }


    @Override
    public void displayTask(Task task) {
        if(task == null) {
            TaskTitleTv.setText(R.string.not_available_text);
            return;
        }
        if(task.getIsImportant()) {
            TaskTitleTv.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.important_text_size));
        } else {
            TaskTitleTv.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.normal_text_size));
        }

        TaskTitleTv.setText(task.getTitle());
        TaskDescriptionTv.setText(task.getDescription());
        importantChkBx.setActivated(task.getIsImportant());
        startTv.setText("Start Date: " + task.getStartDate());
        dueTv.setText("Due Date: " + task.getDueDate());
    }

    @Override
    public void displayNextTask(Task task) {
        nextTaskTv.setText(task.getTitle());
    }

    @Override
    public void handleAdd(int i){
        adapter.notifyItemInserted(i);
        recyclerView.scrollToPosition(0);
    }

    public void refreshPeople(List<Task> all) {
        if(all != null) {
            // replace the existing list
            adapter.setTasks(all);
        } else {
            // just tell the adapter that something has changed
            adapter.notifyDataSetChanged();
        }
    }
}
