package com.example.chatarina.todolist.interfaces;

import com.example.chatarina.todolist.models.Task;
/**
 * Created by Chatarina on 4/3/17.
 */

public interface IPickerView {
    void handleSelection(Task task);
}
