package com.example.chatarina.todolist.interfaces;


import android.content.Intent;
import com.example.chatarina.todolist.viewmodels.EditTaskViewModel;
/**
 * Created by Chatarina on 2/22/17.
 */

public interface IEditView {
    void displayTitle(int title);
    void updateViewWithViewModel(EditTaskViewModel vm);
    void returnResult(EditTaskViewModel viewModel);
    void displayInvalid(EditTaskViewModel viewModel);
    void setInitialFields(EditTaskViewModel viewModel);

    void sendTaskBack(Intent intent);
}
