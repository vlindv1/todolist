package com.example.chatarina.todolist.interfaces;

/**
 * Created by Chatarina on 2/20/17.
 */
import com.example.chatarina.todolist.models.Task;
import java.util.List;

public interface IView {
    void displayTask(Task task);
    void displayNextTask(Task task);
    void showAddView();
    void showEditView();
    void handleDelete(int position);
    void handleAdd(int i);
    void handleEdit();



}
