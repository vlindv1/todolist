package com.example.chatarina.todolist.interfaces;

import android.content.Intent;
import android.text.Editable;
import com.example.chatarina.todolist.models.Task;

/**
 * Created by Chatarina on 2/22/17.
 */

public interface IEditPresenter {
    void showTitle(int requestCode);
    void updateTaskDescription(String text);
    void updateTaskTitle(String text);
    void updateStartDate(String text);
    void updateDueDate(String text);
    void validateModel();
    void saveTask(String title, String artist, boolean isComplete, boolean isImportant, String startDate, String dueDate);
    void setModel(Task task);
    void setViewModel(Intent intent);
    void save();
}
