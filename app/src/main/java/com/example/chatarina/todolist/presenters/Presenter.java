package com.example.chatarina.todolist.presenters;


import java.util.List;

import com.example.chatarina.todolist.interfaces.IDataSource;
import com.example.chatarina.todolist.interfaces.IModel;
import com.example.chatarina.todolist.interfaces.IPresenter;
import com.example.chatarina.todolist.interfaces.IView;
import com.example.chatarina.todolist.models.Task;
import com.example.chatarina.todolist.interfaces.IEditPresenter;
import com.example.chatarina.todolist.interfaces.IEventEmitter;


/**
 * Created by Chatarina on 2/20/17.
 */

public class Presenter implements IPresenter {

    IModel model;
    IView view;
    IDataSource<Task> db;
    IEditPresenter editTaskPresenter;

    int currentTaskIndex;
//old constructor for fragments
//    public Presenter(IView view, IModel model) {
//        this.model = model;
//        this.view = view;
//        currentTaskIndex = 0;
//        view.displayTask(getCurrentTask());
//    }
    public Presenter (IView view, IDataSource<Task> db){
        this.view = view;
        this.db = db;
    }
    @Override
    public void moveToPrevTask() {
        List<Task> tasks = getItems();
        currentTaskIndex--;
        if (currentTaskIndex < 0) {
            currentTaskIndex = tasks.size() - 1;
        }
        view.displayTask(getCurrentTask());
    }

    @Override
    public void moveToNextTask() {
        List<Task> tasks = getItems();
        currentTaskIndex++;
        if (currentTaskIndex >= tasks.size()) {
            currentTaskIndex = 0;
        }
        view.displayTask(getCurrentTask());
    }

    @Override
    public Task getCurrentTask() {
        List<Task> tasks = getItems();
        if (tasks.size() == 0) {
            return null;
        }
        return tasks.get(currentTaskIndex);
    }

    @Override
    public void markCurrentTaskImportant() {
        Task task = getCurrentTask();
        task.setIsImportant(!task.getIsImportant());
        view.displayTask(task);
    }

    @Override
    public Task getNextTask() {
        return getItems().get(currentTaskIndex + 1);
    }

    @Override
    public void showAddOrEditView(Task task){
        if(editTaskPresenter == null && task == null) {
            view.showAddView();
        }
        else if (editTaskPresenter == null && task != null){
            view.showEditView();
        }
        else {
            List<Task> tasks = db.getAll();
            Task editTask = tasks.get(currentTaskIndex);
            editTaskPresenter.setModel(editTask);
        }
    }
    @Override
    public void handleClick(Task task, int adapterPosition) {
        currentTaskIndex = adapterPosition;
        showAddOrEditView(task);
    }

    @Override
    public List<Task> getItems() {
        return db.getAll();
    }

    @Override
    public void handleLongPress(int position) {
        db.delete(position);
        view.handleDelete(position);
    }

    @Override
    public void handleMove(int from, int to) {
        model.move(from, to);
    }

    @Override
    public void handleAddClick(String title, String description, Boolean isComplete, Boolean isImportant, String startDate, String dueDate) {
        db.add(title, description, startDate, dueDate);
        view.handleAdd(0);
    }
    @Override
    public void handleSaveClick(int id, String title, String description, Boolean isComplete, Boolean isImportant, String startDate, String dueDate){
        db.update(id, title, description, startDate, dueDate);

//        List<Task> tasks = getItems();
//        Task task = getCurrentTask();
//        task.setTitle(title);
//        task.setDescription(description);
//        task.setIsComplete(isComplete);
//        task.setIsImportant(isImportant);
//        task.setStartDate(startDate);
//        task.setDueDate(dueDate);
//        tasks.add(task);
        view.handleEdit();
    }

    public void setEditPresenter(IEditPresenter presenter) {
        editTaskPresenter = presenter;
    }
    public void refresh(){
        view.handleEdit();
    }
}
    /*
    @Override
    public void restoreState(int current_task){
        currentTaskIndex = current_task;
        view.displayTask(getCurrentTask());
    }

    @Override
    public int GetState(){
        return currentSongIndex;
    }
     */


