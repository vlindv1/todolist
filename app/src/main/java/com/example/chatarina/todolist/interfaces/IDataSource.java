package com.example.chatarina.todolist.interfaces;

import java.util.List;

/**
 * Created by Chatarina on 4/3/17.
 */
    /**
     * Any object that supplies data to the view should implement this interface
     * @param <T>
     */
    public interface IDataSource<T> {
        List<T> getAll();
        void add(String title, String description, String startDate, String dueDate);
        void update(int id, String title, String description, String startDate, String dueDate);
        void delete(int i);
        void close();
    }

