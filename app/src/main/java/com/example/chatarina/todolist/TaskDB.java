package com.example.chatarina.todolist;

/**
 * Created by Chatarina on 4/3/17.
 */

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.chatarina.todolist.interfaces.IDataSource;
import com.example.chatarina.todolist.models.Task;

import java.util.ArrayList;
import java.util.List;

import java.util.UUID;
import java.util.concurrent.Callable;

/**
 * Created by Chatarina on 4/3/17.
 */

public class TaskDB extends SQLiteOpenHelper implements IDataSource<Task> {
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "tasks_db";
    private static final String TABLE_TASKS = "tasks";
    // Todos Table Columns names
    private static final String KEY_ID = "id";
    private static final String KEY_TITLE = "title";
    private static final String KEY_DESCRIPTION = "description";
    private static final String KEY_STARTDATE = "startdate";
    private static final String KEY_DUEDATE = "duedate";


    private static final String SQL_CREATE_ENTRIES =
            "CREATE TABLE " + TABLE_TASKS + " (" +
                    KEY_ID + " INTEGER PRIMARY KEY," +
                    KEY_TITLE + " TEXT," +
                    KEY_DESCRIPTION + " TEXT," +
                    KEY_STARTDATE + " TEXT," +
                    KEY_DUEDATE + " TEXT)";

    private static final String SQL_DELETE_ENTRIES =
            "DROP TABLE IF EXISTS " + TABLE_TASKS;

    private SQLiteDatabase db;

    public TaskDB(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public void close() {
        db.close();
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(SQL_CREATE_ENTRIES);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {
        onCreate(sqLiteDatabase);
    }

    @Override
    public List<Task> getAll() {
        final SQLiteDatabase db = this.getReadableDatabase();
        // Define a projection that specifies which columns from the database
        // you will actually use after this query.
        final String[] projection = {
                KEY_ID,
                KEY_TITLE,
                KEY_DESCRIPTION,
                KEY_STARTDATE,
                KEY_DUEDATE
        };
        final Cursor cursor = db.query(
                TABLE_TASKS,                     // The table to query
                projection,                               // The columns to return
                null,                                // The columns for the WHERE clause
                null,                            // The values for the WHERE clause
                null,                                     // don't group the rows
                null,                                     // don't filter by row groups
                null                                 // The sort order
        );
        List<Task> tasks = new ArrayList<>();
        while(cursor.moveToNext()) {
            tasks.add(new Task(cursor.getInt(0),cursor.getString(1), cursor.getString(2), false, false, cursor.getString(3),cursor.getString(4)));
            System.out.println("#" + cursor.getInt(0) + " - " + cursor.getString(1) + " - " + cursor.getString(2) + " - " + cursor.getString(3) + " - " + cursor.getString(4));
        }
        cursor.close();
        return tasks;
    }
    /**
     * Adds a Person to the database
     *
     * @param task
     */
    @Override
    public void add(final String title,final String description, final String startDate, final String dueDate) {
        final SQLiteDatabase db = this.getWritableDatabase();
        final ContentValues values = new ContentValues();
        values.put(KEY_TITLE, title);
        values.put(KEY_DESCRIPTION, description);
        values.put(KEY_STARTDATE, startDate);
        values.put(KEY_DUEDATE, dueDate);
        Thread tDb = new Thread(new Runnable(){
            public void run(){
                db.insert(TABLE_TASKS, null, values);
                db.close();
            }
        });
        tDb.start();
    }

    /**
     * Updates a Person in the database
     *
     * @param task
     */
    @Override
    public void update(final int id, final String title, final String description, final String startDate, final String dueDate) {

        final SQLiteDatabase db = this.getReadableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_TITLE, title);
        values.put(KEY_DESCRIPTION, description);
        values.put(KEY_STARTDATE, startDate);
        values.put(KEY_DUEDATE, dueDate);

        Thread tDb = new Thread(new Runnable() {
            public void run() {
                db.execSQL("UPDATE " + TABLE_TASKS
                        + " SET " + KEY_TITLE + " = \'" + title + "\', "
                        + KEY_DESCRIPTION + " = \'" + description +
                        "\', " + KEY_STARTDATE + " = \'" + startDate + "\', "
                        + KEY_DUEDATE + " = \'" + dueDate + "\' WHERE " + KEY_ID + " = " + id);
            }
        });
        tDb.start();
    }


    private ContentValues makeCV(Task task) {
        ContentValues cv = new ContentValues();
        cv.put(KEY_TITLE, task.getTitle());
        cv.put(KEY_DESCRIPTION, task.getDescription());
        cv.put(KEY_STARTDATE, task.getStartDate());
        cv.put(KEY_DUEDATE, task.getDueDate());
        return cv;
    }

    @Override
    public void delete(final int id) {
        final SQLiteDatabase db = this.getWritableDatabase();
        Thread tDb = new Thread(new Runnable(){
            public void run(){
                db.execSQL("DELETE FROM " + TABLE_TASKS + " WHERE id = " + id);
            }
        });
        tDb.start();
    }
}