package com.example.chatarina.todolist.interfaces;

/**
 * Created by Chatarina on 3/27/17.
 */

public interface IEventListener {
    void onEvent(IEventEmitter.IEvent event);
}
