package com.example.chatarina.todolist;

import com.example.chatarina.todolist.interfaces.IModel;
import com.example.chatarina.todolist.interfaces.ITaskService;
import com.example.chatarina.todolist.models.TaskModel;
import com.example.chatarina.todolist.services.TaskService;

/**
 * Created by Chatarina on 2/22/17.
 */

public class Singleton {
    private static Singleton instance;
    public static Singleton getInstance(){
        if(instance == null){
            instance = new Singleton();
        }
        return instance;
    }
    private ITaskService taskService;
    private Singleton(){
        taskService = new TaskService();
    }
}
