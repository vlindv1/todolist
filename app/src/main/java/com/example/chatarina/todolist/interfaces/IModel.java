package com.example.chatarina.todolist.interfaces;

/**
 * Created by Chatarina on 2/20/17.
 */
import java.util.List;

import com.example.chatarina.todolist.models.Task;

public interface IModel {
    List<Task> getTasks();

    void delete(int position);

    int move(int adapterPosition);

    void move(int from, int to);

    void add(Task s);
}
