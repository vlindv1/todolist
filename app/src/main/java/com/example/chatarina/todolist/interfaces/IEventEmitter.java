package com.example.chatarina.todolist.interfaces;

/**
 * Created by Chatarina on 3/27/17.
 */

public interface IEventEmitter {
    void subscribe(IEventListener event);

    interface IEvent<T> {
        T getType();
        Object getValue();
    }
}
