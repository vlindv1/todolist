package com.example.chatarina.todolist.models;

import java.util.ArrayList;
import java.util.List;

import com.example.chatarina.todolist.interfaces.IModel;
import com.example.chatarina.todolist.interfaces.ITaskService;

/**
 * Created by Chatarina on 2/20/17.
 */

public class TaskModel implements IModel{
    List<Task> tasks;

    public TaskModel(ITaskService taskService){
        tasks = new ArrayList<>();
        tasks.add(new Task());
        tasks = taskService.getTasks();
    }
    @Override
    public List<Task> getTasks() { return tasks; }

    @Override
    public void delete(int position) {
        // deletes the item at position
        tasks.remove(position);
    }

    @Override
    public int move(int adapterPosition) {
        // this method arbitrarily moves an item to position 11. no reason.
        Task item = tasks.remove(adapterPosition);
        tasks.add(11, item);
        return 11;
    }

    @Override
    public void move(int from, int to) {
        // moves the item at 'from' to the position 'to'
        Task item = tasks.remove(from);
        tasks.add(to, item);
    }

    @Override
    public void add(Task s) {
        // adds a new Thing to the beginning of the list
        tasks.add(0, s);
    }
}
