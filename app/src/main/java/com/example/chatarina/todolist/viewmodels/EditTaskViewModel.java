package com.example.chatarina.todolist.viewmodels;

import android.content.Intent;
/**
 * Created by Chatarina on 2/22/17.
 */

public class EditTaskViewModel {
    public String title;
    public String description;
    public boolean isComplete;
    public boolean isImportant;
    public String startDate;
    public String dueDate;

    public boolean validate(){
        return title != null && description != null;
    }
    public Intent makeIntent(){
        Intent intent = new Intent();
        intent.putExtra("TITLE", title);
        intent.putExtra("DESCRIPTION", description);
        intent.putExtra("ISCOMPLETE", isComplete);
        intent.putExtra("ISIMPORTANT", isImportant);
        intent.putExtra("STARTDATE", startDate);
        intent.putExtra("DUEDATE", dueDate);
        return intent;
    }
}
