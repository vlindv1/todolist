package com.example.chatarina.todolist.interfaces;

/**
 * Created by Chatarina on 2/20/17.
 */
import java.util.List;
import com.example.chatarina.todolist.models.Task;

public interface ITaskService {
    List<Task> getTasks();

}
