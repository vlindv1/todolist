package com.example.chatarina.todolist.interfaces;

/**
 * Created by Chatarina on 2/20/17.
 */
import com.example.chatarina.todolist.models.Task;

import java.util.List;

public interface IPresenter {
    void moveToPrevTask();
    void moveToNextTask();
    Task getCurrentTask();
    void markCurrentTaskImportant();
    Task getNextTask();

    void showAddOrEditView(Task task);

    void handleClick(Task text, int adapterPosition);

    List<Task> getItems();

    void handleLongPress(int position);

    void handleMove(int from, int to);

    void handleAddClick(String title, String description, Boolean isComplete, Boolean isImportant, String startDate, String dueDate);

    void handleSaveClick(int id, String title, String description, Boolean isComplete, Boolean isImportant, String startDate, String dueDate);

}
