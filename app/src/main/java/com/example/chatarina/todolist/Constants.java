package com.example.chatarina.todolist;

/**
 * Created by Chatarina on 2/22/17.
 */

public class Constants {
    public static final int ADD_TASK_REQUEST_CODE = 1;
    public static final int EDIT_TASK_REQUEST_CODE = 2;
}
