package com.example.chatarina.todolist;

import android.app.Activity;
import android.content.res.Configuration;
import android.os.PersistableBundle;
import android.content.Intent;
import android.support.v4.app.FragmentManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.example.chatarina.todolist.fragments.EditFragment;
import com.example.chatarina.todolist.fragments.MainFragment;
import com.example.chatarina.todolist.interfaces.IDataSource;
import com.example.chatarina.todolist.presenters.EditTaskPresenter;
import com.example.chatarina.todolist.presenters.Presenter;
import com.example.chatarina.todolist.interfaces.ITaskService;
import com.example.chatarina.todolist.models.TaskModel;
import com.example.chatarina.todolist.services.TaskService;


/**
 * Created by Chatarina on 2/20/17.
 */

public class MainActivity extends AppCompatActivity {

    private static final String TAG = MainActivity.class.getSimpleName();

    // the presenter
    Presenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.port_main_act);

        FragmentManager fm = getSupportFragmentManager();
        MainFragment mainFragment = (MainFragment)fm.findFragmentById(R.id.mainFragment);

        //ITaskService taskService = TaskService.getInstance();
        //TaskModel tModel = new TaskModel(taskService);
        TaskDB taskDb = new TaskDB(this);
        presenter = new Presenter(mainFragment, taskDb);
        //presenter = new Presenter(mainFragment, tModel);
        mainFragment.setPresenter(presenter);
        presenter.setEditPresenter(null);

        if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
            EditFragment editFragment = (EditFragment)fm.findFragmentById(R.id.editFragment);
            EditTaskPresenter editPresenter = new EditTaskPresenter(editFragment, taskDb);
            editFragment.setPresenter(editPresenter);

            presenter.setEditPresenter(editPresenter);
            editPresenter.setMainPresenter(presenter);
        }

    }
    @Override
    protected void onPause() {
        super.onPause();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == RESULT_OK) {
            if (requestCode == Constants.ADD_TASK_REQUEST_CODE) {
                //presenter.handleAddClick(data.getStringExtra("TITLE"), data.getStringExtra("DESCRIPTION"), data.getBooleanExtra("ISCOMPLETE", false), data.getBooleanExtra("ISIMPORTANT", false), data.getStringExtra("STARTDATE"), data.getStringExtra("DUEDATE"));
                presenter.refresh();
            }
            if (requestCode == Constants.EDIT_TASK_REQUEST_CODE) {
                //presenter.handleSaveClick(data.getStringExtra("TITLE"), data.getStringExtra("DESCRIPTION"), data.getBooleanExtra("ISCOMPLETE", false), data.getBooleanExtra("ISIMPORTANT", false), data.getStringExtra("STARTDATE"), data.getStringExtra("DUEDATE"));
                presenter.refresh();
            }
        }
    }
    @Override
    public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {
        super.onSaveInstanceState(outState, outPersistentState);
        // config change. saving some state
        outState.putString("THING", "something");
    }
}