package com.example.chatarina.todolist;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.chatarina.todolist.interfaces.IEditPresenter;
import com.example.chatarina.todolist.interfaces.IEditView;
import com.example.chatarina.todolist.models.TaskModel;
import com.example.chatarina.todolist.presenters.EditTaskPresenter;
import com.example.chatarina.todolist.services.TaskService;
import com.example.chatarina.todolist.viewmodels.EditTaskViewModel;

/**
 * Created by Chatarina on 2/22/17.
 */

public class EditTaskActivity extends AppCompatActivity implements IEditView, View.OnClickListener{
    //presenter
    EditTaskPresenter presenter;

    //widgets
    TextView titleTv;
    TextView viewModelTv;
    EditText titleEt;
    Button imptBox;
    EditText startDate;
    EditText dueDate;
    EditText descriptionEt;
    Button saveBtn;

    private static final String TAG = EditTaskActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edit_task_layout);
        bindView();

        presenter = new EditTaskPresenter(this, new TaskDB(this));
        presenter.showTitle(getIntent().getIntExtra("START_REASON", Constants.ADD_TASK_REQUEST_CODE));
        presenter.setViewModel(getIntent());
    }
    private void bindView(){
        titleTv = (TextView)findViewById(R.id.title_text);
        titleEt = (EditText)findViewById(R.id.titleEditText);
        descriptionEt = (EditText)findViewById(R.id.descriptionEditText);
        imptBox = (Button)findViewById(R.id.imptCheckBox);
        startDate = (EditText)findViewById(R.id.startViewEdit);
        dueDate = (EditText)findViewById(R.id.dueViewEdit);
        //viewModelTv = (TextView)findViewById(R.id.debug_model);
        saveBtn = (Button)findViewById(R.id.save_button);

        saveBtn.setOnClickListener(this);
        titleEt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2){

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2){

            }
            @Override
            public void afterTextChanged(Editable editable){
                presenter.updateTaskTitle(editable.toString());
            }
        });
        descriptionEt.addTextChangedListener(new TextWatcher(){
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2){

            }
            @Override
            public void onTextChanged(CharSequence charSequence, int i , int i1, int i2){

            }
            @Override
            public void afterTextChanged(Editable editable){
                presenter.updateTaskDescription(editable.toString());
            }
        });
        startDate.addTextChangedListener(new TextWatcher(){
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2){

            }
            @Override
            public void onTextChanged(CharSequence charSequence, int i , int i1, int i2){

            }
            @Override
            public void afterTextChanged(Editable editable){
                presenter.updateStartDate(editable.toString());
            }
        });
        dueDate.addTextChangedListener(new TextWatcher(){
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2){

            }
            @Override
            public void onTextChanged(CharSequence charSequence, int i , int i1, int i2){

            }
            @Override
            public void afterTextChanged(Editable editable){
                presenter.updateDueDate(editable.toString());
            }
        });

    }

    public static Intent newIntent(Context context){
        Intent intent = new Intent (context, EditTaskActivity.class);
        return intent;
    }

    @Override
    public void displayTitle(int title){
        titleTv.setText(title);
    }

    @Override
    public void updateViewWithViewModel(EditTaskViewModel vm){
//        viewModelTv.setText("Title: " + vm.title + "\nDescription: " + vm.description + "\nStart Date: " + vm.startDate + "\nDue Date: " + vm.dueDate);
    }
    @Override
    public void returnResult(EditTaskViewModel viewModel){
        Intent intent = viewModel.makeIntent();
        setResult(RESULT_OK, intent);
        finish();
    }

    @Override
    public void displayInvalid(EditTaskViewModel viewModel){
        viewModelTv.setText("View model is not valid");
    }

    @Override
    public void setInitialFields(EditTaskViewModel viewModel){
        titleEt.setText(viewModel.title);
        descriptionEt.setText(viewModel.description);
        startDate.setText(viewModel.startDate);
        dueDate.setText(viewModel.dueDate);
    }

    @Override
    public void sendTaskBack(Intent intent){
        setResult(RESULT_OK, intent);
        finish();
    }

    @Override
    public void onClick(View view){
        switch(view.getId()){
            case R.id.save_button:
                presenter.validateModel();
                Log.d(TAG, titleEt.getText().toString() + " " + descriptionEt.getText().toString() + " " + startDate.getText().toString() + " " + dueDate.getText().toString());
                presenter.saveTask(titleEt.getText().toString(), descriptionEt.getText().toString(), false, false, startDate.getText().toString(), dueDate.getText().toString());
                break;
        }
    }
}
