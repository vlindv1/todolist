package com.example.chatarina.todolist.models;

import java.util.UUID;

/**
 * Created by Chatarina on 2/20/17.
 */

public class Task {
    private String title;
    private String description;
    private boolean isComplete;
    private boolean isImportant;
    private String startDate;
    private String dueDate;
    private int id;
    public Task(){
        isComplete = false;
        isImportant = false;
    }
    public Task(int id, String title, String description, boolean isComplete, boolean isImportant, String startDate, String dueDate){
        this.id = id;
        this.title = title;
        this.description = description;
        this.isComplete = isComplete;
        this.isImportant = isImportant;
        this.startDate = startDate;
        this.dueDate = dueDate;
    }
    public int getId() {return id; }
    public void setId(int id) { this.id = id; }
    public String toString() {
        return title;
    }
    public String getTitle(){
        return title;
    }
    public void setTitle(String title){
        this.title = title;
    }
    public String getDescription(){
        return description;
    }
    public void setDescription(String description){
        this.description = description;
    }
    public boolean getIsComplete(){
        return isComplete;
    }
    public void setIsComplete(boolean isComplete){
        this.isComplete = isComplete;
    }
    public String getStartDate(){
        return startDate;
    }
    public void setStartDate(String startDate){
        this.startDate = startDate;
    }
    public String getDueDate(){
        if (this.dueDate == null){
            return null;
        }
        return dueDate;
    }
    public void setIsImportant(boolean isImportant){
        this.isImportant = isImportant;
    }
    public boolean getIsImportant(){
        return isImportant;
    }
    public void setDueDate(String dueDate){
        this.dueDate = dueDate;
    }
}

