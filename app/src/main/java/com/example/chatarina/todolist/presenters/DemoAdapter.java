package com.example.chatarina.todolist.presenters;


import android.content.ClipData;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.Image;
import android.os.AsyncTask;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.chatarina.todolist.fragments.MainFragment;
import com.example.chatarina.todolist.interfaces.IPresenter;
import com.example.chatarina.todolist.models.Task;
import com.example.chatarina.todolist.R;

import java.io.InputStream;
import java.util.List;

/**
 * Created by randy on 2/18/17.
 */

public class DemoAdapter extends RecyclerView.Adapter<DemoAdapter.DemoViewHolder> {

    IPresenter presenter;
    List<Task> tasks;

    // inject (pass in) the presenter.
    // the presenter is where we get the data for this adapater.
    // the presenter holds the model, so we ask the presenter for data,
    // the presenter then asks the model for data, and the model asks the datasource
    public DemoAdapter(final IPresenter presenter) {
        this.presenter = presenter;
        this.tasks = presenter.getItems();
    }

    public void setTasks(List<Task> tasks) {
        this.tasks = tasks;
        this.notifyDataSetChanged();
    }

    @Override
    public DemoViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // inflate the view holder here
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_list_item, parent, false);
        return new DemoViewHolder(view);
    }

    @Override
    public void onBindViewHolder(DemoViewHolder holder, int position) {
        // find the right item in the collection using the position and bind the view to the Thing
        Task item = presenter.getItems().get(position);
        holder.bind(item);
    }

    @Override
    public int getItemCount() {
        return presenter.getItems().size();
    }

    public void delete(int adapterPosition) {
        presenter.handleLongPress(adapterPosition);
    }

    class DemoViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {

        TextView tv;
        TextView tv2;
        ImageView imgView;

        Task thing;

        public DemoViewHolder(View itemView) {
            super(itemView);
            tv = (TextView) itemView.findViewById(R.id.listItemTextView);
            tv2 = (TextView) itemView.findViewById(R.id.listItemTextView2);

        }

        // binds an item to the view
        public void bind(Task item) {
            thing = item;
            imgView = (ImageView) itemView.findViewById(R.id.taskImg);
            new DownloadImageTask(imgView)
                    .execute("https://api.adorable.io/avatars/285/abott@adorable.png");
            tv.setText(item.getTitle());
            tv2.setText(item.getDescription());
            tv.setOnClickListener(this);
            tv.setOnLongClickListener(this);
        }

        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.listItemTextView:
                    // delegate clicks to the presenter
                    presenter.handleClick(thing, getAdapterPosition());
                    break;
            }
        }

        @Override
        public boolean onLongClick(View view) {
            switch (view.getId()) {
                case R.id.listItemTextView:
                    // delegate long clicks to the presenter
                    presenter.handleLongPress(getAdapterPosition());
            }
            return false;
        }

        private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
            ImageView bmImage;

            public DownloadImageTask(ImageView bmImage) {
                this.bmImage = bmImage;
            }

            protected Bitmap doInBackground(String... urls) {
                String urldisplay = urls[0];
                Bitmap mIcon11 = null;
                try {
                    InputStream in = new java.net.URL(urldisplay).openStream();
                    mIcon11 = BitmapFactory.decodeStream(in);
                } catch (Exception e) {
                    Log.e("Error", e.getMessage());
                    e.printStackTrace();
                }
                return mIcon11;
            }

            protected void onPostExecute(Bitmap result) {
                bmImage.setImageBitmap(result);
            }
        }
    }
}

