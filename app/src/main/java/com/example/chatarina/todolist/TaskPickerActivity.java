//package com.example.chatarina.todolist;
//
//import android.content.Intent;
//import android.os.Bundle;
//import android.support.v7.app.AppCompatActivity;
//import android.support.v7.widget.LinearLayoutManager;
//import android.support.v7.widget.RecyclerView;
//
//import com.example.chatarina.todolist.models.Task;
//import com.example.chatarina.todolist.interfaces.IPickerView;
//import com.example.chatarina.todolist.presenters.PickerPresenter;
//import com.example.chatarina.todolist.presenters.PickerAdapter;
//
///**
// * Created by Chatarina on 4/3/17.
// */
//
//public class TaskPickerActivity extends AppCompatActivity implements IPickerView {
//    PickerAdapter adapter;
//    PickerPresenter presenter;
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.task_pager);
//
//        // presenter and adapter.
//        presenter = new PickerPresenter(this, new TaskDB(this));
//        adapter = new PickerAdapter(presenter);
//
//        // the recyclerview
//        RecyclerView rv = (RecyclerView)findViewById(R.id.recyclerView);
//        rv.setLayoutManager(new LinearLayoutManager(this));
//        rv.setAdapter(adapter);
//    }
//
//    @Override
//    protected void onStop() {
//        super.onStop();
//        presenter.finish();
//    }
//
//    @Override
//    public void handleSelection(Task task) {
//        Intent intent = new Intent();
//        intent.putExtra(TaskContract.TASK_TITLE_EXTRA, task.getTitle());
//        intent.putExtra(TaskContract.TASK_DESCRIPTION_EXTRA, task.getDescription());
//        intent.putExtra(TaskContract.TASK_STARTDATE_EXTRA, task.getStartDate());
//        intent.putExtra(TaskContract.TASK_DUEDATE_EXTRA, task.getDueDate());
//        setResult(RESULT_OK, intent);
//        finish();
//    }
//}
