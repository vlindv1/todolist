package com.example.chatarina.todolist.presenters;

import android.content.Intent;
import android.util.Log;

import com.example.chatarina.todolist.Constants;
import com.example.chatarina.todolist.MainActivity;
import com.example.chatarina.todolist.R;

import com.example.chatarina.todolist.interfaces.IDataSource;
import com.example.chatarina.todolist.interfaces.IEditPresenter;
import com.example.chatarina.todolist.interfaces.IEditView;
import com.example.chatarina.todolist.interfaces.IModel;
import com.example.chatarina.todolist.viewmodels.EditTaskViewModel;
import com.example.chatarina.todolist.interfaces.IPresenter;
import com.example.chatarina.todolist.models.Task;

/**
 * Created by Chatarina on 2/22/17.
 */

public class EditTaskPresenter implements IEditPresenter {
    //private final IModel model;
    EditTaskViewModel viewModel;
    IDataSource<Task> db;
    IEditView view;
    IPresenter mainPresenter;
    Task currentTask;

    private static final String TAG = EditTaskPresenter.class.getSimpleName();

    public EditTaskPresenter(IEditView view, IDataSource<Task> db){
        this.view = view;
        this.db = db;
        viewModel = new EditTaskViewModel();
    }

    @Override
    public void showTitle(int requestCode){
        if(requestCode == Constants.ADD_TASK_REQUEST_CODE) {
            view.displayTitle(R.string.add_task_btn);
        }
        else {
            view.displayTitle(R.string.edit_task_btn);
        }
    }

    @Override
    public void updateTaskDescription(String text){
        viewModel.description = text;
        view.updateViewWithViewModel(viewModel);
    }
    @Override
    public void updateStartDate(String text){
        viewModel.startDate = text;
        view.updateViewWithViewModel(viewModel);
    }
    @Override
    public void updateDueDate(String text){
        viewModel.dueDate = text;
        view.updateViewWithViewModel(viewModel);
    }

    @Override
    public void updateTaskTitle(String text){
        viewModel.title = text;
        view.updateViewWithViewModel(viewModel);
    }

    @Override
    public void validateModel(){
        if(viewModel.validate()) {
            view.returnResult(viewModel);
        }
        else{
            view.displayInvalid(viewModel);
        }
    }

    @Override
    public void saveTask(String title, String description, boolean isComplete, boolean isImportant, String startDate, String dueDate){
        db.add(title, description, startDate, dueDate);
        Intent intent = new Intent();
        Log.d(TAG, startDate + " " + dueDate);
        intent.putExtra("TITLE", title);
        intent.putExtra("DESCRIPTION", description);
        intent.putExtra("ISCOMPLETE", isComplete);
        intent.putExtra("ISIMPORTANT", isImportant);
        intent.putExtra("STARTDATE", startDate);
        intent.putExtra("DUEDATE", dueDate);
        view.sendTaskBack(intent);
    }

    @Override
    public void setViewModel(Intent intent){
        if(intent.getIntExtra("START_REASON", Constants.ADD_TASK_REQUEST_CODE) == Constants.EDIT_TASK_REQUEST_CODE){
            viewModel.description = intent.getStringExtra("DESCRIPTION");
            Log.d(TAG, viewModel.description);
            viewModel.title = intent.getStringExtra("TITLE");
            viewModel.startDate = intent.getStringExtra("STARTDATE");
            viewModel.dueDate = intent.getStringExtra("DUEDATE");
            view.updateViewWithViewModel(viewModel);
            view.setInitialFields(viewModel);
        }
    }
    public void setMainPresenter(IPresenter mainPresenter) {
        this.mainPresenter = mainPresenter;
    }

    @Override
    public void setModel(Task task){
        if (task != null) {
            viewModel.description = task.getDescription();
            viewModel.title = task.getTitle();
            viewModel.startDate = task.getStartDate();
            viewModel.dueDate = task.getDueDate();
        }
        else {
            viewModel.description = "";
            viewModel.title = "";
            viewModel.startDate = "";
            viewModel.dueDate = "";
        }
        view.updateViewWithViewModel(viewModel);
        view.setInitialFields(viewModel);
        currentTask = task;
    }

    @Override
    public void save() {
        if(mainPresenter == null) {
            // if there are no listeners (no shared presenter)
            // then send the text back to the main activity
            saveTask(viewModel.title, viewModel.description, false, false, viewModel.startDate, viewModel.dueDate);
        } else {
            // otherwise, clear the edit text and emit the SAVE event
            if (currentTask == null)
                mainPresenter.handleAddClick(viewModel.title, viewModel.description, false, false, viewModel.startDate, viewModel.dueDate);
            else{
                mainPresenter.handleSaveClick(currentTask.getId(), viewModel.title, viewModel.description, false, false, viewModel.startDate, viewModel.dueDate);
            }
        }
    }
}
