package com.example.chatarina.todolist.presenters;

import java.util.List;
import com.example.chatarina.todolist.models.Task;
import com.example.chatarina.todolist.interfaces.IDataSource;
import com.example.chatarina.todolist.interfaces.IPickerView;
/**
 * Created by Chatarina on 4/3/17.
 */

public class PickerPresenter {
    private final IPickerView view;
    private IDataSource<Task> db;

    public PickerPresenter(IPickerView view, IDataSource<Task> dataSource) {
        this.db = dataSource;
        this.view = view;
    }

    public List<Task> getTasks() {
        return db.getAll();
    }

    public void selectPerson(Task task) {
        view.handleSelection(task);
    }

    public void finish() {
        db.close();
    }
}
