package com.example.chatarina.todolist.interfaces;

/**
 * Created by Chatarina on 2/22/17.
 */

public interface IEditModel {
    void setTitle(String text);
    void setDescription(String text);
}
