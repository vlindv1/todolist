package com.example.chatarina.todolist.presenters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.List;
import java.util.Locale;

import com.example.chatarina.todolist.R;
import com.example.chatarina.todolist.models.Task;

/**
 * Created by Chatarina on 4/3/17.
 */

public class PickerAdapter extends RecyclerView.Adapter<PickerAdapter.PickerViewHolder> {

    private List<Task> tasks;
    private PickerPresenter presenter;

    public PickerAdapter(PickerPresenter presenter) {
        this.presenter = presenter;
        // fetch the list once, this way we aren't always querying the database for the list
        this.tasks = presenter.getTasks();
    }

    @Override
    public PickerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.edit_task_layout, parent, false);
        return new PickerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(PickerViewHolder holder, int position) {
        Task task = tasks.get(position);
        holder.bind(task);
    }

    @Override
    public int getItemCount() {
        return tasks.size();
    }

    class PickerViewHolder extends RecyclerView.ViewHolder {

        EditText titleEt;
        EditText startDate;
        EditText dueDate;
        EditText descriptionEt;
        Button saveBtn;
        Task task;

        public PickerViewHolder(View itemView) {
            super(itemView);
            titleEt = (EditText)itemView.findViewById(R.id.titleEditText);
            descriptionEt = (EditText)itemView.findViewById(R.id.descriptionEditText);
            startDate = (EditText)itemView.findViewById(R.id.startViewEdit);
            dueDate = (EditText)itemView.findViewById(R.id.dueViewEdit);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    presenter.selectPerson(task);
                }
            });
        }

        public void bind(Task task) {
            this.task = task;
            titleEt.setText(task.getTitle());
            descriptionEt.setText(task.getDescription());
            startDate.setText(task.getStartDate());
            dueDate.setText(task.getDueDate());

            // using the default locale as suggested by the linter.
            //ageTv.setText(String.format(Locale.getDefault(), "%d", task.getDescription()));
        }
    }
}
