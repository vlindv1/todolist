package com.example.chatarina.todolist.services;

import java.util.ArrayList;
import java.util.List;

import com.example.chatarina.todolist.interfaces.ITaskService;
import com.example.chatarina.todolist.models.Task;

/**
 * Created by Chatarina on 2/20/17.
 */

public class TaskService implements ITaskService {
    List<Task> tasks;

    private static TaskService ourInstance = new TaskService();

    public static TaskService getInstance() {
        return ourInstance;
    }

    public TaskService(){
        tasks = new ArrayList<>();
//        Task task = new Task("Homework", "Program 1", false, true, "February 12, 2017", "February 20, 2017");
//        tasks.add(task);
//        task = new Task("Study for Test", "Section 1.1-1.9", false, true, "February 20, 2017", "February 21, 2017");
//        tasks.add(task);
//        task = new Task("Read Chapter 2", "Section 2, 3, 4", false, true, "February 20, 2017", "February 23, 2017");
//        tasks.add(task);
//        task = new Task("Do COSC 431 Hw", "Hw 4", false, true, "March 6, 2017", "March 6, 2017");
//        tasks.add(task);
//        task = new Task("Study for quiz", "Section 1.0", false, false, "March 6", "march 7");
//        tasks.add(task);
//        task = new Task("Stop being lazy", "Ok", false, false, "ASAP", "ASAP");
//        tasks.add(task);
//        task = new Task("homework", "math", false, false, "march 4, 2017", "march 5, 2017");
//        tasks.add(task);
//        task = new Task("notes", "comp sci", false, false, "march 1, 2017", "March 5, 2017");
//        tasks.add(task);
//        task = new Task("cram for exam", "science", false, false, "March 4, 2017", "March 10, 2017");
//        tasks.add(task);
//        task = new Task("do more work", "for school", false, false, "March 1, 2017", "March 10, 2017");
//        tasks.add(task);
//        task = new Task("ok this is redundant", "i think i get it", false, false, "March 20, 2017", "March 31, 2017");
//        tasks.add(task);
//        task = new Task("last one for sure", "i think this is 12", false, false, "March 10, 2017", "March 30, 2017");
//        tasks.add(task);

    }
    public List<Task> getTasks() {return tasks; }

}
